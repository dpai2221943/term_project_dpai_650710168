<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interested</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar" style="background-color: #309f7e;">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <h2>ㅤTerm Project</h2>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.html">ㅤㅤProfile</a>
                        <a class="nav-link active" aria-current="page" href="interested.html">ㅤㅤInterested</a>
                        <a class="nav-link active" aria-current="page" href="about_su.html">ㅤㅤAbout Silpakorn
                            University</a>
                        <a class="nav-link active" aria-current="page" href="db.php">ㅤㅤDatabase</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid" style="height: 100vh; background: linear-gradient(45deg, #3498db, #2ecc71);">
        <?php

            $servername = "db";
            $username = "devops";
            $password = "devops101";

            $dbhandle = mysqli_connect($servername, $username, $password);
            $selected = mysqli_select_db($dbhandle, "titanic");

            echo "Connected database server<br>";
            echo "Selected database";
        ?>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
        crossorigin="anonymous"></script>

</body>

<style>
    body {
        background: linear-gradient(45deg, #3498db, #2ecc71);
    }
    h2 {
        color: white;
    }
</style>

</html>
